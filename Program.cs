﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Image = iTextSharp.text.Image;

namespace PdfSplitPage
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length != 1) 
            {
                Console.WriteLine("Especificar el archivo de origen");
                return;
            }

            if (!File.Exists(args[0]))
            {
                Console.WriteLine($"El archivo {args[0]} no existe");
                return;
            }

            var originalFile = args[0];
            var transformedFile = Path.Combine(
                Path.GetDirectoryName(originalFile), 
                Path.GetFileNameWithoutExtension(originalFile) + "_Imprimible" + Path.GetExtension(originalFile)
            );

            var target = new Document(iTextSharp.text.PageSize.A5);
            // target.pdf is A5 Portrait format

            /// source.pdf is A4 Landscape format
            var reader = new PdfReader(originalFile);
            var writer = PdfWriter.GetInstance(target, new FileStream(transformedFile, FileMode.Create));
            
            target.Open();
            writer.Open();

            var cb = writer.DirectContent;

            Console.Write("Copiando paginas...");

            for (int i = 1; i <= reader.NumberOfPages; i++)
            {
               

                var size = reader.GetPageSize(i);

                if(size.Width < 400)
                {
                    var imageBytes = GetImage(reader.GetPageN(i), reader);
                    var ms = new MemoryStream(imageBytes);



                    var msImage = System.Drawing.Image.FromStream(ms);


                    target.NewPage();
                    Console.Write(".");
                    var image = Image.GetInstance(msImage, ImageFormat.Jpeg);
                    image.SetAbsolutePosition(0, 0);
                    image.ScalePercent(25);

                    cb.AddImage(image);

                    if(i == 1)
                    {
                        target.NewPage();
                        Console.Write(".");
                        target.Add(new Chunk());
                    }
                }
                else
                {

                    var imageBytes = GetImage(reader.GetPageN(i), reader);


                    var ms = new MemoryStream(imageBytes);

                        
                        
                    var msImage = System.Drawing.Image.FromStream(ms);
                    var temp = (System.Drawing.Bitmap)msImage;
                    var bmap = (System.Drawing.Bitmap)temp.Clone();
                    var rect1 = new System.Drawing.Rectangle(0, 0, msImage.Width / 2, msImage.Height);
                    var msImage1 = (System.Drawing.Bitmap)bmap.Clone(rect1, bmap.PixelFormat);

                    var rect2 = new System.Drawing.Rectangle((msImage.Width / 2), 0, msImage.Width / 2, msImage.Height);
                    var msImage2 = (System.Drawing.Bitmap)bmap.Clone(rect2, bmap.PixelFormat);



                    target.NewPage();
                    Console.Write(".");
                    var image1 = Image.GetInstance(msImage1, ImageFormat.Jpeg);
                    image1.SetAbsolutePosition(0, 0);
                    image1.ScalePercent(25);

                    cb.AddImage(image1);

                    target.NewPage();
                    Console.Write(".");
                    var image2 = Image.GetInstance(msImage2, ImageFormat.Jpeg);
                    image2.SetAbsolutePosition(0, 0);
                    image2.ScalePercent(25);

                    cb.AddImage(image2);


                }

            }



            int n = reader.NumberOfPages;
            iTextSharp.text.Rectangle psize = reader.GetPageSize(1);
            float width = psize.Width;
            float height = psize.Height;

            target.Close();
            writer.Close();
            reader.Close();

            // page1 created - an half of source A4 landscape page


            Console.WriteLine();
            Console.WriteLine("PDF Transformado");
        }


        public static byte[] GetImage(PdfDictionary pg, PdfReader reader)
        {
            
            var res = PdfReader.GetPdfObject(pg.Get(new PdfName("Resources"))) as PdfDictionary;
            var xobj = PdfReader.GetPdfObject(res.Get(new PdfName("XObject"))) as PdfDictionary;
            if (xobj == null) return null;

            var keys = xobj.Keys.OfType<PdfName>().ToList();
            if (keys.Count == 0) return null;

            var obj = xobj.Get(keys.ElementAt(0));
            if (!obj.IsIndirect()) return null;

            var tg = PdfReader.GetPdfObject(obj) as PdfDictionary;
            var type = PdfReader.GetPdfObject(tg.Get(new PdfName("Subtype"))) as PdfName;
            if (!new PdfName("Image").Equals(type)) return null;
            
            int XrefIndex = (obj as PdfIndirectReference).Number;
            var pdfStream = reader.GetPdfObject(XrefIndex) as PrStream;
            var data = PdfReader.GetStreamBytesRaw(pdfStream);
            return data;
        }
    }
}
