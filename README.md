# Compilador de PDF Snell 2007 Hindi para principiantes

El PDF original es muy dificil de imprimir como libro. Por eso con este programa se puede convertir

`
PDF Original 58.7 MB
`

## PDF Original


![PDF Original](docs/images/original.png "PDF Original")



## PDF Transformado



![PDF Transformado](docs/images/compilado.png "PDF Transformado")


## Cómo usar

Luego de compilar

```
dotnet PdfSplitPage.dll "C:/pdf_origen.pdf"
```